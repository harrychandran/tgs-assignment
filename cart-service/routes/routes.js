import { addCart, updateCart, deleteCart, findAll } from '../controllers/controller';
import { checkToken, checkUserRole } from '../middleware'
import express from 'express';
const CartRouter = express.Router();

// Add a new Pizza //only admin can do it
CartRouter.post('/', checkToken, checkUserRole, addCart);

// Retrieve all Pizzas
CartRouter.get('/',checkToken, checkUserRole, findAll);


// Update a Pizza with PizzaID
CartRouter.put('/:productId', checkToken, checkUserRole, updateCart);

// Delete a Pizza with PizzaID
CartRouter.delete('/:productId', checkToken, checkUserRole, deleteCart);

module.exports = CartRouter;