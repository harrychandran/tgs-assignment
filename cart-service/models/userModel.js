import mongoose from 'mongoose';
import bcrypt from 'bcryptjs';

const UserSchema = mongoose.Schema({
    role: { type: String, required: true },
    name: { type: String, unique: true, required: true },
    password: { type: String, required: true }
}, {
    timestamps: true
});

//hashing a password before saving it to the database
UserSchema.pre('save', function (next) {
    var user = this;
    bcrypt.hash(user.password, 10, function (err, hash){
      if (err) {
        return next(err);
      }
      user.password = hash;
      next();
    })
  });

export default mongoose.model('User', UserSchema);