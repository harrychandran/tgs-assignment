import redisClient from '../redis';


// Create and Save a new pizza
const addCart = (req, res) => {
    if(req.isAdmin) {
        return res.status(400).send({
            message: "Only user can add the pizza"
        });
    }

    redisClient.get(req.user._id, function(err, data) {
        if(err) {
            res.status(403).send({msg:"there is some issue to add to cart"})
        }else{ 
            let stoerdData = data;
            if (!data) {
                data = [];
                stoerdData = JSON.stringify(data);
            }
            stoerdData = JSON.parse(stoerdData);
            stoerdData.push(req.body),

            setData(req.user._id, stoerdData, function (err, reply) {
                if (err) {
                    res.status(403).send({msg:"there is some issue to add to cart"})
                } else {
                    res.json(reply);
                }
             });
        }
     });
};


// get current cart items
const findAll = (req, res) => {
    if(req.isAdmin) {
        return res.status(400).send({
            message: "Only user can update the pizza"
        });
    }

    redisClient.get(req.user._id, function(err, data) {
        if(err) {
            res.status(403).send({msg:"there is some issue to add to cart"})
        }else{
            if(data !=='undefined'){
                res.json(JSON.parse(data))
            }else{
                res.status(403).send({msg:"there is some issue to add to cart"})
            }
        }
     })
};


// Update a product identified by the productID in the request
const updateCart = (req, res) => {
    if(req.isAdmin) {
        return res.status(400).send({
            message: "Only user can add the pizza"
        });
    }

    redisClient.get(req.user._id, function(err, data) {
        if(err) {
            res.status(403).send({msg:"there is some issue to add to cart"})
        }else{ 
            let stoerdData = JSON.parse(data);
            stoerdData.forEach(function(item, index) {
                if(item._id == req.params.productId) {
                    item.quantity= parseInt(req.body.quantity);
                  //result.total=(quantity*result.price);
                }    
              });

            setData(req.user._id, stoerdData, function (err, reply) {
                if (err) {
                    res.status(403).send({msg:"there is some issue to add to cart"});
                } else {
                    res.json(reply);
                }
             });
        }
     });
};

// Delete a product with the specified productId in the request
const deleteCart = (req, res) => {
    if(req.isAdmin) {
        return res.status(400).send({
            message: "Only admin can update the pizza"
        });
    }

    redisClient.get(req.user._id, function(err, data) {
        if(err) {
            res.status(403).send({msg:"there is some issue to add to cart"})
        }else{ 
            let jsonObj = JSON.parse(data).filter(item =>  { 
                return item._id !== req.params.productId;
            });

            setData(req.user._id, jsonObj, function (err, reply) {
                if (err) {
                    res.status(403).send({msg:"there is some issue to add to cart"})
                } else {
                    res.json(reply);
                }
             });
        }
     })
};




function setData(id, data, callback) {
    redisClient.set(id, JSON.stringify(data),function (err, reply) {
        if (err) {
            callback(err)
        } else {
            callback(null, data);
        } 
    });
}

module.exports = {
    addCart,
    updateCart,
    deleteCart,
    findAll
}