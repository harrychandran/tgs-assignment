//resolvers.js
import Pizza from './models/model';
export const resolvers = {
    Query: {
        async allPizza() {
            return await Pizza.find();
        }
    }
};