import express from 'express';
import bodyParser from 'body-parser';
import helmet from 'helmet';
import userRouter from './routes/routes';
import path from 'path';
import cors from 'cors';

// Configuring the database
import dbConfig from './config/database.config.js';
import mongoose from 'mongoose';

// create express app
const app = express();

app.use(helmet());

app.use(cors());


// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))


// parse requests of content-type - application/json
app.use(bodyParser.json())


//app.use(express.static(path.join(__dirname, './client')));


mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// define a simple route
// app.get('/', function (req, res) {
//     res.send('Hello World!')
// })

// get All routes
app.use('/auth', userRouter);

const port = process.env.PORT || 3001
// listen for requests
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});