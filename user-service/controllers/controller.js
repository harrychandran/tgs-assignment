import User from '../models/model.js';
import jwt from 'jsonwebtoken';
import config from '../../config/auth.config.js';
import bcrypt from 'bcryptjs';


// Create and Save a new user
const register = (req, res) => {
    // Validate request
    if(!req.body.role) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Create a Note
    const user = new User({
        role: req.body.role, 
        name: req.body.name,
        password: req.body.password
    });

    // Save Note in the database
    user.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// login
const login = (req, res) => {
    let name = req.body.name;
    let password = req.body.password;
    User.findOne({ name: name })
    .then(user => {
        if (!user) return res.status(400).json({ msg: "User not exist" });
        if (password) {
            bcrypt.compare(password, user.password, (err, data) => {
                //if error than throw error
                if (err) throw err
                //if both match than you can do anything
                if (data) {
                    let token = jwt.sign({name: name},
                        config.secret,
                        { expiresIn: '24h' // expires in 24 hours
                        }
                      );
                      // return the JWT token for the future API calls    
                     return res.json({
                        success: true,
                        message: 'Authentication successful!',
                        token: token
                      });
                 //   return res.status(200).json({ msg: "Login success" })
                } else {
                    return res.status(401).json({ msg: "Invalid credencial" })
                }

            });
        } else {
          res.send(400).json({
            success: false,
            message: 'Authentication failed! Please check the request'
          });
        }
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};


// Retrieve and return all notes from the database.
const findAll = (req, res) => {
    User.find()
    .then(users => {
        res.send(users);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};


module.exports = {
    register: register,
    login: login,
    findAll:findAll
}