
import { register, login, findAll } from '../controllers/controller';
import express from 'express';
const userRouter = express.Router();

// Create a new User
userRouter.post('/registration', register);

// Login
userRouter.post('/login', login);

userRouter.get('/findAll', findAll);

module.exports = userRouter;