import express from 'express';
import bodyParser from 'body-parser';
import graphlHTTP from 'express-graphql';
import schema from './schema';
import helmet from 'helmet';
import productRouter from './routes/routes';
import cors from 'cors';

// Configuring the database
import dbConfig from './config/database.config.js';
import mongoose from 'mongoose';

// create express app
const app = express();

app.use(helmet());

app.use(cors());


// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))


// parse requests of content-type - application/json
app.use(bodyParser.json())


//app.use(express.static(path.join(__dirname, './client')));


mongoose.Promise = global.Promise;

// Connecting to the database
mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

// get All routes
app.use('/product', productRouter);

app.use('/graphql', graphlHTTP({
    schema: schema,
    graphiql: true
}));


const port = process.env.PORT || 3003
// listen for requests
app.listen(port, () => {
    console.log(`Server is listening on port ${port}`);
});