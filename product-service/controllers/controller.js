import Pizza from '../models/model';

// Create and Save a new pizza
const createProduct = (req, res) => {
    if(req.user.role !== 'admin') {
        return res.status(400).send({
            message: "Only admon can add the pizza"
        });
    }
      // Create a pizza
    const pizza = new Pizza({
        name: req.body.name,
        description: req.body.description,
        type: req.body.type,
        toppings: req.body.toppings,
        price: req.body.price
    });

    // Save pizza in the database
    pizza.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Product."
        });
    });
};


// Update a product identified by the productID in the request
const updateProduct = (req, res) => {
    if(req.user.role !== 'admin') {
        return res.status(400).send({
            message: "Only admin can update the pizza"
        });
    }

    Pizza.findByIdAndUpdate(req.params.productId, {$set: req.body}).then(product => {
        if(!product) {
            return res.status(404).send({
                message: "Product not found with id " + req.params.productId
            });
        }
        res.send('Product Successfully Updated');
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Product not found with id " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Error updating Product with id " + req.params.productId
        });
    });
};

// Delete a product with the specified productId in the request
const deleteProduct = (req, res) => {
    if(req.user.role !== 'admin') {
        return res.status(400).send({
            message: "Only admin can update the pizza"
        });
    }
    Pizza.findByIdAndRemove(req.params.productId)
    .then(product => {
        if(!product) {
            return res.status(404).send({
                message: "Product not found with id " + req.params.productId
            });
        }
        res.send({message: "Product deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Product not found with id " + req.params.productId
            });                
        }
        return res.status(500).send({
            message: "Could not delete Product with id " + req.params.productId
        });
    });
};

// Retrieve and return all pizza from the database.
const findAll = (req, res) => {
    let data = {user: req.user,pizza:null};
    Pizza.find()
    .then(pizza => {
        data.pizza= pizza;
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};

module.exports = {
    createProduct,
    updateProduct,
    deleteProduct,
    findAll
}