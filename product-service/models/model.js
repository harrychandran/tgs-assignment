import mongoose from 'mongoose';

const PizzaSchema = mongoose.Schema({
    name: { type : String , unique : true, required : true, dropDups: true },
    description: String,
    type: String,
    toppings: String,
    price: Number
}, {
    timestamps: true
});

export default mongoose.model('Pizza', PizzaSchema);