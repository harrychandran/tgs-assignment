import { createProduct, updateProduct, deleteProduct, findAll } from '../controllers/controller';
import { checkToken, checkUserRole } from '../middleware'
import express from 'express';
const ProductRouter = express.Router();

// Add a new Pizza //only admin can do it
ProductRouter.post('/pizza', checkToken, checkUserRole, createProduct);

// Retrieve all Pizzas
ProductRouter.get('/pizza',checkToken, checkUserRole, findAll);


// Update a Pizza with PizzaID
ProductRouter.put('/pizza/:productId', checkToken, checkUserRole, updateProduct);

// Delete a Pizza with PizzaID
ProductRouter.delete('/pizza/:productId', checkToken, checkUserRole, deleteProduct);

module.exports = ProductRouter;