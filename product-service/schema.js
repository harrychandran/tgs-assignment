//schema.js
import {
    makeExecutableSchema
} from 'graphql-tools';

import {
    resolvers
} from './resolvers';

const typeDefs = `
type Pizza {
  _id: ID!
  name: String!
  description: String
  type: String,
  toppings: String,
  price: Int
 }
type Query {
  allPizza: [Pizza]
 }
`;
const schema = makeExecutableSchema({
    typeDefs,
    resolvers
});
export default schema;