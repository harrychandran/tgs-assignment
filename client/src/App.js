import React from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from "react-router-dom";

import Login from "./components/Login";
import SignUp from "./components/SignUp";
import Products from "./components/Products";
import CartList from "./components/CartList";

function App() {
  return (
      <Router>
        <div className="App">
          <nav className="navbar navbar-expand-lg navbar-light">
            <div className="container">
              <Link className="navbar-brand" to={"/signin"}>Pizza Hot</Link>
              <div className="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul  className="navbar-nav">
                  <li className="nav-item">
                    <Link className="nav-link" to={"/account"}>Account</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/products"}>Product</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/cart"}>My Cart</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/orders"}>Order History</Link>
                  </li>
                </ul>
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item">
                    <Link className="nav-link" to={"/signin"}>Login</Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to={"/signup"}>Sign up</Link>
                  </li>
                </ul>
              </div>
            </div>
          </nav>

          <div className="auth-wrapper container">
            <div className="auth-inner">
              <Switch>
                <Route exact path='/' component={Login} />
                <Route path="/signin" component={Login} />
                <Route path="/signup" component={SignUp} />
                <Route path="/products" component={Products} />
                <Route path="/cart" component={CartList} />
              </Switch>
            </div>
          </div>
        </div>
      </Router>
  );
}

export default App;
