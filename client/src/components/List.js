import React, {Component} from 'react';
import Loading from './Loading';
import gql from 'graphql-tag';
import { graphql, Query } from 'react-apollo'

const query = gql`
    {
        allPizza {
            _id,
            name,
            description,
            price,
            type,
            toppings
        }
    }
`;
class List extends Component {
    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          cartItems: []
        };
        this.cartUrl = 'http://localhost:3004/cart';
        this.productUrl = 'http://localhost:3003/product';
      }

    addToCart = (item) => {
        console.log(item)
        const data = {
            _id: item._id,
            name: item.name,
            description: item.description,
            type: item.type,
            price: item.description,
            toppings: item.toppings,
            quantity: 1
        }
        const options = {
            method: 'post',
            headers: new Headers({
                'content-type': 'application/json',
                'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoidXNlciIsImlhdCI6MTU3NDYwMzgwOCwiZXhwIjoxNTc0NjkwMjA4fQ.ZUw1s9oNf7B4s4mZORKCrCGWGYnY17IOs62SBj5W6mw', 
            }),
            body: JSON.stringify(data)
        };
        fetch(`${this.cartUrl}`, options)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: false,
              cartItems: result
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }

    getPizzaList = (product) => {
        const { name, description, price, } = product;
        const staticimage = 'data:image/svg+xml;charset=UTF-8,<svg%20width%3D"286"%20height%3D"180"%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20viewBox%3D"0%200%20286%20180"%20preserveAspectRatio%3D"none"><defs><style%20type%3D"text%2Fcss">%23holder_16e9d76f1bd%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20<%2Fstyle><%2Fdefs><g%20id%3D"holder_16e9d76f1bd"><rect%20width%3D"286"%20height%3D"180"%20fill%3D"%23777"><%2Frect><g><text%20x%3D"107.1875"%20y%3D"96.6">286x180<%2Ftext><%2Fg><%2Fg><%2Fsvg>'
        return (
            <div className="card">
                <img className="card-img-top" src={staticimage} alt="Card image cap"/>
                <div className="card-body">
                    <h5 className="card-title">{name}</h5>
                    <p className="card-text">{description}</p>
                    <div className="addCartContainer">
                        <h5 className="card-title">{price}</h5>
                        <a href="#" className="btn btn-primary" onClick={() => this.addToCart(product)}>Add</a>
                    </div>
                   
                </div>
            </div>
        )
    }

    render() {
        return (
              <div className="App">
                <Query query={query}>
                    {({ data, loading }) => {                 
                        if (loading) {
                            return <div><Loading/></div>;
                        }
                        const { allPizza } = data;
                        return (
                            <div className="row">
                                {allPizza.map((product, index) => (
                                    <div className='col-sm-3 productList'>{this.getPizzaList(product)}</div>
                                ))}
                            </div>
                        );
                    }}
                </Query>
              </div>
          );
    }
}
export default List;
