import React, {Component} from 'react';
import Loading from './Loading';

class CartList extends Component {
    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          cartItems: []
        };
        this.cartUrl = 'http://localhost:3004/cart';
      }

    componentDidMount() {
        const options = {
            method: 'delete',
            headers: new Headers({
                'content-type': 'application/json',
                'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoidXNlciIsImlhdCI6MTU3NDYwMzgwOCwiZXhwIjoxNTc0NjkwMjA4fQ.ZUw1s9oNf7B4s4mZORKCrCGWGYnY17IOs62SBj5W6mw', 
            })
        };
        fetch(`${this.cartUrl}/findAll`, options)
          .then(res => res.json())
          .then(
            (result) => {
              this.setState({
                isLoaded: false,
                cartItems: result
              });
            },
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
    }

    onChangeQty = (e, id) => {
        this.putItem(id, e.target.value);
    }

    getCartList = (item, i) => {
        const { name, description, price, quantity, _id } = item;
        const staticimage = 'data:image/svg+xml;charset=UTF-8,<svg%20width%3D"286"%20height%3D"180"%20xmlns%3D"http%3A%2F%2Fwww.w3.org%2F2000%2Fsvg"%20viewBox%3D"0%200%20286%20180"%20preserveAspectRatio%3D"none"><defs><style%20type%3D"text%2Fcss">%23holder_16e9d76f1bd%20text%20%7B%20fill%3Argba(255%2C255%2C255%2C.75)%3Bfont-weight%3Anormal%3Bfont-family%3AHelvetica%2C%20monospace%3Bfont-size%3A14pt%20%7D%20<%2Fstyle><%2Fdefs><g%20id%3D"holder_16e9d76f1bd"><rect%20width%3D"286"%20height%3D"180"%20fill%3D"%23777"><%2Frect><g><text%20x%3D"107.1875"%20y%3D"96.6">286x180<%2Ftext><%2Fg><%2Fg><%2Fsvg>'
        return (
            <tr>
                <td data-th="Product">
                    <div className="row">
                        <div className="col-sm-2 hidden-xs-down"><img src="http://placehold.it/100x100" alt="..." className="img-responsive"/></div>
                        <div className="col-sm-10">
                            <h4 className="nomargin">{name}</h4>
                            <p>{description}</p>
                        </div>
                    </div>
                </td>
                <td data-th="Price">Rs {price}</td>
                <td data-th="Quantity">
                    <input type="number" className="form-control text-center" defaultValue={quantity} onChange={(e)=>this.onChangeQty(e,_id)} />
                </td>
                <td data-th="Subtotal" className="text-center">1.99</td>
                <td className="actions" data-th="">
                    {/* <button className="btn btn-info btn-sm" onClick={() => this.editItem(_id)}><i className="fa fa-refresh"></i></button> */}
                    <button className="btn btn-danger btn-sm" onClick={() => this.deleteItem(_id)}><i className="fa fa-trash-o"></i></button>								
                </td>
            </tr>
        )
    }

    putItem = (id, value) => {
        const options = {
            method: 'put',
            headers: new Headers({
                'content-type': 'application/json',
                'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoidXNlciIsImlhdCI6MTU3NDYwMzgwOCwiZXhwIjoxNTc0NjkwMjA4fQ.ZUw1s9oNf7B4s4mZORKCrCGWGYnY17IOs62SBj5W6mw', 
            }),
            body: JSON.stringify({quantity: value})
        };
        fetch(`${this.cartUrl}/${id}`, options)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: false,
              cartItems: result
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }

    deleteItem = (id) => {
        const options = {
            method: 'delete',
            headers: new Headers({
                'content-type': 'application/json',
                'token': 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJuYW1lIjoidXNlciIsImlhdCI6MTU3NDYwMzgwOCwiZXhwIjoxNTc0NjkwMjA4fQ.ZUw1s9oNf7B4s4mZORKCrCGWGYnY17IOs62SBj5W6mw', 
            })
        };
        fetch(`${this.cartUrl}/${id}`, options)
        .then(res => res.json())
        .then(
          (result) => {
            this.setState({
              isLoaded: false,
              cartItems: result
            });
          },
          (error) => {
            this.setState({
              isLoaded: true,
              error
            });
          }
        )
    }

    render() {
        const { isLoaded, cartItems } = this.state;
        return (
              <div className="container">
                <table id="cart" className="table table-hover table-condensed">
                    <thead>
                        <tr>
                            <th style={{width: '30%'}}>Product</th>
                            <th style={{width:'10%'}}>Price</th>
                            <th style={{width:'8%'}}>Quantity</th>
                            <th style={{width:'22%'}} className="text-center">Subtotal</th>
                            <th style={{width:'10%'}}></th>
                        </tr>
                    </thead>

                    {isLoaded && (
                        <tbody><Loading/></tbody>
                    )}
                    {cartItems && cartItems.map((item, index) => (
                        <tbody>{this.getCartList(item, index)}</tbody>
                    ))}
                    <tfoot>
                        <tr className="d-block d-sm-none">
                            <td className="text-center"><strong>Total 1.99</strong></td>
                        </tr>
                        <tr>
                            <td><a href="/products" className="btn btn-warning"><i className="fa fa-angle-left"></i> Continue Shopping</a></td>
                            <td colSpan="2" className="hidden-xs-down"></td>
                            <td className="d-none d-sm-block text-center"><strong>Total $1.99</strong></td>
                            <td><a href="#" className="btn btn-success btn-block">Checkout <i className="fa fa-angle-right"></i></a></td>
                        </tr>
                    </tfoot>
                </table>
            </div>
          );
    }
}
export default CartList;
