import React, {Component} from 'react';
import List from './List';
import { createHttpLink } from "apollo-link-http";
import ApolloClient from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import { InMemoryCache } from 'apollo-cache-inmemory'

const client = new ApolloClient({
  link: createHttpLink({ uri: "http://localhost:3003/graphql" }),
  cache: new InMemoryCache()
});

class Products extends Component {

  render() {
    return (
      <ApolloProvider client={client}>
        <div className="App">
          <header className="App-header">
                <h2>Products</h2>
          </header>
            <List />
        </div>
      </ApolloProvider>
    );
  }
}

export default Products;
