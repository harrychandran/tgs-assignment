import React, { Component } from "react";


import '../style/main.css';

export default class SignUp extends Component {
    render() {
        return (
            <form className='formContainer'>
                <h3>Sign Up</h3>

                <div className="form-group">
                    <label>Role</label>
                    <input type="text" className="form-control" placeholder="Role" />
                </div>

                <div className="form-group">
                    <label>Name</label>
                    <input type="text" className="form-control" placeholder="Name" />
                </div>

                <div className="form-group">
                    <label>Password</label>
                    <input type="password" className="form-control" placeholder="Enter password" />
                </div>

                <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
                <p className="forgot-password text-right">
                    Already registered <a href="/signin">sign in?</a>
                </p>
            </form>
        );
    }
}