const Order = require('./models/model.js');
import jwt from 'jsonwebtoken';
import config from '../config/auth.config'

let checkToken = (req, res, next) => {
  let token = req.headers['token'] || req.headers['authorization']; // Express headers are auto converted to lowercase
  if (token) {
    jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return res.json({
          success: false,
          message: 'Token is not valid'
        });
      } else {
        req.decoded = decoded;
        next();
      }
    });
  } else {
    return res.json({
      success: false,
      message: 'Auth token is not supplied'
    });
  }
};


let checkUserRole = (req, res, next) => {
    User.findOne({ name: req.decoded.name })
    .then(user => {
        req.user = user;
        next();
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred"
        });
    });
};

module.exports = {
  checkToken: checkToken,
  checkUserRole: checkUserRole
}