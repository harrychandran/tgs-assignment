import express from 'express';
import bodyParser from 'body-parser';
import schema from './models/model';
import helmet from 'helmet';
import orderRouter from './routes/routes';
import cors from 'cors';

// Configuring the database
import mongoose from 'mongoose';
const port = process.env.PORT || 3002;

// create express app
const app = express();

app.use(helmet());

app.use(cors());


// parse requests of content-type - application/x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: true }))


// parse requests of content-type - application/json
app.use(bodyParser.json())

mongoose.Promise = global.Promise;


// define a simple route
app.get('/', function (req, res) {
    res.send('Order Service!')
})

// get All routes
app.use('/order', orderRouter);

//database connections
require('./dbConnection')
.then(connection => {
    app.listen(port, () => {
        console.log('Order Service is listening at port- ', port);
    });
})