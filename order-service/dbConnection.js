// Configuring the database
import dbConfig from './config/database.config.js';
import mongoose from 'mongoose';

// Connecting to the database
const db = mongoose.connect(dbConfig.url, {
    useNewUrlParser: true
}).then(() => {
    console.log("Successfully connected to the database");    
}).catch(err => {
    console.log('Could not connect to the database. Exiting now...', err);
    process.exit();
});

module.exports = db;