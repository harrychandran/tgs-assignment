const Order = require('../models/model.js');

// Create and Save a new pizza
exports.order = (req, res) => {
    const  order=req.body;
    orderData.orderBy=req.body.user

    // Save order in the database
    order.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Product."
        });
    });
};

// Retrieve and return all orders by the user from the database.
exports.orders = (req, res) => {
    let data = {user: req.user,order:null};
    Order.find({ username: req.body.username })
    .then(order => {
        data.order= order;
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
