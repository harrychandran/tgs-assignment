const Mongoose 	= require('mongoose');
const Schema=Mongoose.Schema;
const toppingSchema= Schema({
  toppingId:String,
  toppingType:String,
  price:Number
})
module.exports = toppingSchema;