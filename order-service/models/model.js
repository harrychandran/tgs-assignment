const Mongoose 	= require('mongoose');
const Schema=Mongoose.Schema;
import toppingSchema from "./toppingModel";
import selectedProductSchema from "./toppingModel"

const orderSchema = Schema({
  orderBy: String,
  name : String,
  phone : String,
  address : String,
  orderedPizza:[selectedProductSchema],
  subtotal : Number,
  tax : Number,
  totalPrice : Number,
  createdOn: { type: Date, default: Date.now }
});

module.exports = Mongoose.model('Order', orderSchema);