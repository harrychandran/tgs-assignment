const Mongoose 	= require('mongoose');
const Schema=Mongoose.Schema;

const selectedProductSchema= Schema({
  size : String,
  productName : String,
  productId:String,
  toppings : [toppingSchema],
  quantity : Number,
  price:Number
})

module.exports=selectedProductSchema;