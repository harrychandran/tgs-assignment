
import { orderPizza, orders } from '../controllers/controller';
import { checkToken, checkUserRole } from '../middleware'
import express from 'express';
const OrderRouter = express.Router();

// Add a new Pizza //only admin can do it
OrderRouter.post('/', checkToken, checkUserRole, orderPizza);

// Retrieve all Pizzas
OrderRouter.get('/',checkToken, checkUserRole, orders);


module.exports = OrderRouter;